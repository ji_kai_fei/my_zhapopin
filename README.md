# my_zhapopin

#### 介绍
自己做的一个基于springboot的招聘网站

#### 软件架构
软件架构说明


#### 安装教程

1.  需要Java环境，Maven，IDEA
2.  需要mysql数据库
3.  把项目放到本地后，修改数据库路径，maven配置，jdk版本

#### 使用说明

1.  网站必须进行登录！拦截器会在未登录时跳转到登录页面。验证码登录未实现，穷买不起。暂定伪验证码为666666.（注册也是）
![输入图片说明](https://images.gitee.com/uploads/images/2020/0504/161455_a9adb81b_5384276.png "登录.png")
2.  登录成功后可以进行职位搜索，简历创建，简历投递，生成简历预览等。
①、登录注册：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0504/162724_2e5174b1_5384276.png "登录.png")
②、首页（职位列表不能点，emmm其底层也只是关键字搜索，所以，没写，啊哈！）：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0504/162754_2e10dede_5384276.png "首页以及搜索搜索框.png")
③、职位搜索页面（多条件查询，点击工作名称可进入职位详情页）：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0504/162935_e8b9f84d_5384276.png "职位搜索页面.png")
④、职位详情页（可进行简历投递，查看公司详情）：
![职位详情](https://images.gitee.com/uploads/images/2020/0504/163038_4e9c9c08_5384276.png "职位详情及投递.png")
![投递](https://images.gitee.com/uploads/images/2020/0504/163059_8e47d254_5384276.png "职位投递功能.png")
![公司详情](https://images.gitee.com/uploads/images/2020/0504/163248_60ba6334_5384276.png "公司详情.png")
④、个人中心（简历以及投递记录）：
![简历中心](https://images.gitee.com/uploads/images/2020/0504/163158_3d20b3b2_5384276.png "简历中心.png")
![简历编辑](https://images.gitee.com/uploads/images/2020/0504/163313_d34895fb_5384276.png "简历编辑页面.png")
![简历预览](https://images.gitee.com/uploads/images/2020/0504/163337_0a30a504_5384276.png "简历预览页面.png")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
